﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DissolveScript : MonoBehaviour
{
    Material myMat;
    float dissolveValue;
    private bool dissolve;
    // Start is called before the first frame update
    void Start()
    {
        myMat = GetComponent<MeshRenderer>().material;
        dissolveValue = GetComponent<MeshRenderer>().material.GetFloat("_DsolveValue");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            StartCoroutine(Dissolve());
        }
        if (dissolve)
        {
            myMat.SetFloat("_DsolveValue", dissolveValue += Time.deltaTime * 0.5f);
        }
    }
     IEnumerator Dissolve()
    {
        dissolve = true;

        yield return new WaitForSeconds(3f);

        Destroy(gameObject);


    }
}
