﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DwarfKillScript : MonoBehaviour
{
    private Animator anim;
    private Material mat;
    [SerializeField] private float timeBeforeDissolve;
    [SerializeField] private float timeBeforeDespawn;
    float dissolveValue;
    bool dissolving;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        mat = GetComponentInChildren<SkinnedMeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        dissolveValue = mat.GetFloat("_DsolveValue");
        if (Input.GetKeyDown(KeyCode.K))
        {
            StartCoroutine(DeathDissolve());
        }
        if (dissolving)
        {
            mat.SetFloat("_DsolveValue", dissolveValue += Time.deltaTime);
        }
    }
    IEnumerator DeathDissolve()
    {
        anim.SetTrigger("Death");
        yield return new WaitForSeconds(timeBeforeDissolve);
        dissolving = true;
        yield return new WaitForSeconds(timeBeforeDespawn);
        Destroy(gameObject);
    }
}
