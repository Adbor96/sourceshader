﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMat : MonoBehaviour
{
    public Color[] colors;
    private Color startingColor;
    private int count;
    private Material mat;
    private void Start()
    {
        mat = GetComponent<MeshRenderer>().material;
        startingColor = mat.GetColor("_UnlitColor");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (count != colors.Length)
            {
                count++;
                mat.SetColor("_UnlitColor", colors[count - 1]);
            }
            else
            {
                mat.SetColor("_UnlitColor", startingColor);
                count = 0;
            }
        }
    }
}
